package com.kundan.graph;

import java.util.Set;

public interface Graph<T> {
	
	public boolean addEdge(T fistVertex,T secondVertex,int wait);
	public boolean addVertex(T vertex);
	public void clear();
	public boolean containsEdge(T firstVertex,T secondVertex);
	public boolean containsVertex(Object vertex);
	public Set<T> getNeighbors(T vertex);
	public int getWeight(T firstVertex,T secondVertex);
	public boolean isEmpty();
	public int numberOfEdges();
	public int numberOfVertics();
	public boolean removeEdge(T firstVertex,T secondVertex);
	public int setWeight(T firstVertex, T secondVertex);
	
}
