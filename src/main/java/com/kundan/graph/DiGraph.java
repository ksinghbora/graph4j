package com.kundan.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

public class DiGraph<T> implements Graph<T> {

	private HashMap<T, Integer> vertexMap;
	private ArrayList<VertexInfo> vertexInfos;
	private Stack<Integer> availStack;
	private int numberOfEdges;

	public DiGraph() {
		// TODO:Need to implement
	}

	public void colorWhite() {
		// TODO:Need to implement
	}

	private Edge findEdge(LinkedList<Edge> edgeList, int destination) {
		// TODO:Need to implement
		return null;
	}

	public Color getColor(T vertex) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		return vertexInfo.vertexColor;
	}

	public int getData(T vertex) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		return vertexInfo.dataValue;
	}

	public T getParent(T vertex) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		return vertexInfo.parentVertex;
	}

	private int getVertexInfoIndex(Object vertex) {
		int index = vertexMap.get(vertex);
		return index;

	}

	public int inDegree(T vertex) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		return vertexInfo.inDegree;
	}

	public void initData() {
		// TODO:Need to implement
	}
 
	public int outDegree(T vertex) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		return vertexInfo.edges.size();
	}

	public DiGraph<String> readGraph(String fileName) {
		// TODO:Need to implement
		return null;
	}

	private void removedFixup(int index) {
		// TODO:Need to implement
	}

	public Color setColor(T vertex, Color color) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		Color oldColor = vertexInfo.vertexColor;
		vertexInfo.vertexColor = color;
		return oldColor;
	}

	public int setData(T vertex, int data) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		int oldData = vertexInfo.dataValue;
		vertexInfo.dataValue = data;
		return oldData; 
	}

	public T setParent(T vertex, T parentVertex) {
		int index = getVertexInfoIndex(vertex);
		if (index == -1) {
			throw new IllegalArgumentException(
					"Given vertex not present in graph");
		}
		VertexInfo<T> vertexInfo = vertexInfos.get(index);
		T oldParentVertex = vertexInfo.parentVertex;
		vertexInfo.parentVertex = parentVertex;
		return oldParentVertex; 
	}

	@Override
	public boolean addEdge(T firstVertex, T secondVertex, int weight) {
		int posOne = vertexMap.get(firstVertex);
		int posTwo = vertexMap.get(secondVertex);
		
		if(posOne==-1 || posTwo==-1){
			throw new IllegalArgumentException("both or either of the vertex not present in graph");
		}
		Edge edge = new Edge(posTwo, weight);
		VertexInfo firstVertexInfo = vertexInfos.get(posOne);
		if(firstVertexInfo.edges.contains(edge)){
			return false;
		}
		firstVertexInfo.edges.add(edge);
		VertexInfo secondVertexInfo = vertexInfos.get(posTwo);
		secondVertexInfo.inDegree++;
		numberOfEdges++;
		return true;
	}

	@Override
	public boolean addVertex(T vertex) {
		int availablePos=-1;
		if(availStack.size()>0){
			availablePos = availStack.pop();
		}
		else{
			availablePos = vertexInfos.size();
		}
		vertexMap.put(vertex, availablePos);
		//need to reinitialize vertexInfo (remove old data from object)
		vertexInfos.get(availablePos).vertex = vertex;
		return true;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean containsEdge(T firstVertex, T secondVertex) {
		int posFirst = vertexMap.get(firstVertex);
		int posSecond = vertexMap.get(secondVertex);
		
		if(posFirst==-1 || posSecond==-1){
			throw new IllegalArgumentException("both or either of the vertex not present in graph");
		}
		
		Iterator  iterator = vertexInfos.get(posFirst).edges.iterator();
		
		while (iterator.hasNext()) {
			Edge edge = (Edge) iterator.next();
			if(edge.destination==posSecond){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsVertex(Object vertex) {
		return vertexMap.containsKey(vertex);
	}

	@Override
	public Set<T> getNeighbors(T vertex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getWeight(T firstVertex, T secondVertex) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int numberOfEdges() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int numberOfVertics() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean removeEdge(T firstVertex, T secondVertex) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int setWeight(T firstVertex, T secondVertex) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DiGraph []";
	}

}

class VertexInfo<T> {

	public T vertex;
	public LinkedList<Edge> edges = new LinkedList<Edge>();
	public int inDegree;
	public boolean occupied;
	public Color vertexColor;
	public int dataValue;
	public T parentVertex;

	public VertexInfo(T vertex) {
		this.vertex = vertex;
	}

}

class Edge {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + destination;
		result = prime * result + weight;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Edge)) {
			return false;
		}
		Edge other = (Edge) obj;
		if (destination != other.destination) {
			return false;
		}
		if (weight != other.weight) {
			return false;
		}
		return true;
	}

	int destination;
	int weight;

	public Edge(int destination, int weight) {
		this.destination = destination;
		this.weight = weight;
	}

}
